# modern-gazette
<img src="https://user-images.githubusercontent.com/70860732/113427159-7924d300-93cc-11eb-9e67-5ea755df1320.jpeg" width="50%" align="right">

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/modern-gazette/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/modern-gazette?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/modern-gazette?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/modern-gazette?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/modern-gazette)](https://replit.com/@KennyOliver/modern-gazette)

**Simple news website made using CSS flexbox**

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/modern-gazette)](https://kennyoliver.github.io/modern-gazette)

---
Kenny Oliver ©2021
